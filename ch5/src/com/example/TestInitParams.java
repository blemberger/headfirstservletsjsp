package com.example;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
public class TestInitParams extends HttpServlet
{
	public void doGet(HttpServletRequest request, HttpServletResponse response)  
                                                   throws IOException, ServletException {
		ServletContext ctxt = getServletContext();
		ctxt.log(logResponseState(response.isCommitted()));
		response.setContentType("text/html");
		ctxt.log(logResponseState(response.isCommitted()));
		PrintWriter out = response.getWriter();
		out.println("test init parameters<br>");
		ctxt.log(logResponseState(response.isCommitted()));
		java.util.Enumeration e = getServletConfig().getInitParameterNames();
		while(e.hasMoreElements()) {
			out.println("<br>param name = " + e.nextElement());
		}
		out.println("<br><br>main email is " + getServletConfig().getInitParameter("mainEmail"));
		out.println("<br>admin email is " + getServletConfig().getInitParameter("adminEmail"));
		
		//Get a context-param named "bslCxtParam"
		out.println("<br><br>" + getServletContext().getInitParameter("bslCxtParam"));
		ctxt.log(logResponseState(response.isCommitted()));
		out.flush();
		ctxt.log(logResponseState(response.isCommitted()));
	}
	
	private String logResponseState(boolean com) {
		String msg = "The response is ";
		msg += com ? "" : "not ";
		msg += "committed.";
		return msg;
	}
}