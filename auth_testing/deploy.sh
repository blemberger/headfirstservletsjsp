#!/bin/sh

if [ "$#" -ne 1 ]; then
	echo You need to specify one argument as the app name!
	exit 1
else
	APP_NAME=$1
fi

if [ ! -e ../apache-tomcat-5.5.36/webapps/$APP_NAME ]; then
	mkdir -p ../apache-tomcat-5.5.36/webapps/$APP_NAME/WEB-INF/classes
fi
cp -r classes/ ../apache-tomcat-5.5.36/webapps/$APP_NAME/WEB-INF/classes
cp web/* ../apache-tomcat-5.5.36/webapps/$APP_NAME/
cp etc/web.xml ../apache-tomcat-5.5.36/webapps/$APP_NAME/WEB-INF/
