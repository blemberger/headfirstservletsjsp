package com.example.web;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class AuthServlet extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		boolean authorized = false;
		String authHeader = request.getHeader("Authorization"); // get the first one
		if (authHeader != null) {
			response.setStatus(HttpServletResponse.SC_OK);
			if (authHeader.startsWith("Basic")) {
				String credentials = authHeader.split("\\s")[1];
				if (credentials.equals("YnJpYW46YnJpYW4=")) {
					authorized = true;
				}
			}
		}

		if (authorized) {
			returnAuthorized(response);
		}
		else {
			returnUnauthorized(response);
		}
	}

	private void returnUnauthorized(HttpServletResponse response) {
		response.addHeader("WWW-Authenticate", "Basic realm=\"BSL\"");
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	}

	private void returnAuthorized(HttpServletResponse response) throws IOException {
			PrintWriter out = response.getWriter();
			out.println("<html " +
					"<body>" +
						"<h1 align=center>Hello Brian!</h1>" +
					"</body>"  +
			"</html>");

	}
}
