package com.example.web;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.List;
import com.example.model.BeerExpert;

public class BeerSelect extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		response.addHeader("BSL", "one");
		response.addHeader("BSL", "two");
		
		// response.sendRedirect("http://www.oreilly.com");
		
		String c = request.getParameter("color");
		
		BeerExpert expert = new BeerExpert();
		List<String> result = expert.getBrands(c);
		
		request.setAttribute("styles", result);
		RequestDispatcher view = request.getRequestDispatcher("result.jsp");
		view.forward(request, response);
		getServletContext().log("Came back to servlet!");
	}
}
