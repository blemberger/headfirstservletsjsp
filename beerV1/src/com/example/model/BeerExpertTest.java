package com.example.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.util.List;

public class BeerExpertTest {

	private BeerExpert exp;

	@Before
	public void setUp() throws Exception {
		exp = new BeerExpert();
	}

	@Test
	public void testLightColor() {
		List<String> brands = exp.getBrands("light");
		Assert.assertTrue(brands.contains("Jail Pale Ale"));
		Assert.assertTrue(brands.contains("Gout Stout"));
	}
	
	@Test
	public void testAmberColor() {
		List<String> brands = exp.getBrands("amber");
		Assert.assertTrue(brands.contains("Jack Amber"));
		Assert.assertTrue(brands.contains("Red Moose"));
	}
	
	@Test
	public void testBrownColor() {
		List<String> brands = exp.getBrands("brown");
		Assert.assertTrue(brands.contains("Jail Pale Ale"));
		Assert.assertTrue(brands.contains("Gout Stout"));
	}
	
	@Test
	public void testDarkColor() {
		List<String> brands = exp.getBrands("dark");
		Assert.assertTrue(brands.contains("Jail Pale Ale"));
		Assert.assertTrue(brands.contains("Gout Stout"));
	}

}
