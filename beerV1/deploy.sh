#!/usr/bin/env bash

if [ -z ${CATALINA_VERSION+x} ]; then
   echo You need to set \$CATALINA_VERSION to the version of tomcat you have
   exit 1
fi

if [ "$#" -ne 1 ]; then
	echo You need to specify one argument as the app name!
	exit 1
else
	APP_NAME=$1
fi

if [ ! -e ../apache-tomcat-$CATALINA_VERSION/webapps/$APP_NAME ]; then
	mkdir -p ../apache-tomcat-$CATALINA_VERSION/webapps/$APP_NAME/WEB-INF/classes
fi
# Copy the java classes to the new webapp directory classes dir
cp -r classes/ ../apache-tomcat-$CATALINA_VERSION/webapps/$APP_NAME/WEB-INF/classes

# Copy the static content files (.html, .css, .jsp, etc.) to the webapp directory root
cp web/* ../apache-tomcat-$CATALINA_VERSION/webapps/$APP_NAME/

# Copy the web app deployment descriptor file to the descriptor dir in the web app dir
cp etc/web.xml ../apache-tomcat-$CATALINA_VERSION/webapps/$APP_NAME/WEB-INF/
