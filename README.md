# What is this repository for?

This repo contains code I wrote as part of reading *Head First Servlets and JSP*.

# beerV1

Example that takes up most of Chapter 3

Compiling
---------
From beerv1 directory:

    mkdir classes
    javac -d classes -classpath ../apache-tomcat-5.5.36/common/lib/servlet-api.jar:lib/junit-4.11.jar src/com/example/model/BeerExpert.java src/com/example/model/BeerExpertTest.java src/com/example/web/BeerSelect.java

Deploying
---------
    ./deploy.sh

Running JUnit
-------------

    java -classpath lib/junit-4.11.jar:lib/hamcrest-core-1.3.jar:classes org.junit.runner.JUnitCore com.example.model.BeerExpertTest

URL
---
    http://localhost:8080/beer/form.html

curl
----
Fetching the form page:

    curl -v http://localhost:8080/beer/form.html

Posting to the beer form page:

    curl -v -d @misc/beerpost.txt http://localhost:8080/beer/SelectBeer.do

[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
